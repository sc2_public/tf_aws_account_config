# tf_aws_account_config

Sets up AWS Config in a default Stanford AWS account.

* Creates a Configuration Recorder for a subset of resource types
* Creates a Delivery Channel
* Enables the Configuration Recorder
* Authorizes access from the organization's Configuration Aggregator

## Variables

| Name                 | Type   | Description                                                                                                         | Default                                                                                                                                |
|----------------------|--------|---------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------|
| `role_arn`           | string | AWS Config service linked role                                                                                      | _none_                                                                                                                                 |
| `iam_region`         | string | Region where IAM resources are recorded                                                                             | us-west-2                                                                                                                              |
| `resources`          | list   | Resource types to record in every region                                                                            | AWS::EC2::FlowLog,<br/>AWS::EC2::SecurityGroup,<br/>AWS::EC2::Volume,<br/>AWS::EC2::VPC,<br/>AWS::RDS::DBInstance,<br/>AWS::S3::Bucket |
| `iam_resources`      | list   | Resource types to record in "IAM region"                                                                            | AWS::IAM::User                                                                                                                         |
| `delivery_frequency` | string | How often config changes are delivered | `One_Hour`                                                                                                                                       |

## Outputs

| Name                      | Type   | Description                            |
|---------------------------|--------|----------------------------------------|
| `alias`                   | string | Generated account alias                |
| `config_service_role_arn` | string | Service linked role ARN for AWS Config |

## Usage

This module must be instantiated for each region that requires AWS Config.

```terraform
module "account" {
  source = "git::https://code.stanford.edu/sc2_public/tf_aws_account_default.git"
  alias  = "foo-prod"
}

module "config_us_west_2" {
  source    = "git::https://code.stanford.edu/sc2_public/tf_aws_account_config.git"
  role_arn  = module.account.config_service_role_arn
  providers = {
    aws       = aws.us_west_2
    aws.audit = aws.audit
  }
}
```

## Related Modules

* [AWS Default Setup](https://code.stanford.edu/sc2_public/tf_aws_account_default/)
* [VPC FlowLogs setup](https://code.stanford.edu/sc2_public/tf_aws_account_flowlogs)
* [Guardduty setup](https://code.stanford.edu/sc2_public/tf_aws_account_guardduty)
