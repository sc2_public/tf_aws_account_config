# need ARN from account module
variable "role_arn" {
  description = "ARN of config.amazonaws.com service linked role"
  type        = string
}

variable "iam_region" {
  description = "Region in which IAM resources are recorded"
  type        = string
  default     = "us-west-2"
}

variable "resources" {
  description = "List of resource types to record"
  type        = list
  default     = [
    "AWS::EC2::FlowLog",
    "AWS::EC2::SecurityGroup",
    "AWS::EC2::Volume",
    "AWS::EC2::VPC",
    "AWS::RDS::DBInstance",
    "AWS::S3::Bucket"
  ]
}

variable "iam_resources" {
  description = "List of IAM resource types to record in IAM region"
  type        = list
  default     = [
    "AWS::IAM::User"
  ]
}

variable "delivery_frequency" {
  description = "Frequency of Config updates"
  type        = string
  default     = "One_Hour"
}

locals {
  region    = data.aws_region.current.name
  _region   = replace(data.aws_region.current.name, "-", "_")
  resources = setunion(
    toset(var.resources),
    toset((local.region == var.iam_region) ? var.iam_resources : []))
}
