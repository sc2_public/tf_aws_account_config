#----- AWS CONFIG FOR ACCOUNT -----

# need access to the main audit account to find the config bucket
provider "aws" {
  alias = "audit"
}

resource "aws_config_configuration_recorder" "main" {
  name     = local.region
  role_arn = var.role_arn
  recording_group {
    all_supported = false
    include_global_resource_types = false
    resource_types = local.resources
  }
}

resource "aws_config_delivery_channel" "main" {
  name           = local.region
  s3_bucket_name = data.aws_s3_bucket.config.id

  snapshot_delivery_properties {
    delivery_frequency = var.delivery_frequency
  }

  depends_on     = [
    aws_config_configuration_recorder.main
  ]
}

resource "aws_config_configuration_recorder_status" "main" {
  name       = local.region
  is_enabled = true
  depends_on = [
    aws_config_delivery_channel.main,
  ]
}

# add authorization for audit account aggregator

resource "aws_config_aggregate_authorization" "audit" {
  account_id = data.aws_caller_identity.audit.account_id
  region     = "us-west-2"
}
