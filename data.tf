data "aws_caller_identity" "audit" {
  provider = aws.audit
}

data "aws_region" "current" {}

data "aws_organizations_organization" "main" {}

data "aws_s3_bucket" "config" {
  bucket   = "${data.aws_organizations_organization.main.id}-config"
  provider = aws.audit
}

